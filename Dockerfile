FROM debian:bullseye AS builder

ARG NGINX_VERSION=1.21.6 \
    NGINX_RTMP_VERSION=1.2.2

WORKDIR /usr/local/src

RUN export DEBIAN_FRONTEND=noninteractive \
 && apt-get update \
 && apt-get install -y \
      curl \
      gcc \
      libssl-dev \
      make \
 && curl -JLO https://github.com/arut/nginx-rtmp-module/archive/v${NGINX_RTMP_VERSION}.tar.gz \
 && curl -JLO http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz

RUN tar xf nginx-rtmp-module-${NGINX_RTMP_VERSION}.tar.gz \
 && tar xf nginx-${NGINX_VERSION}.tar.gz \
 && cd nginx-${NGINX_VERSION} \
 && ./configure \
      --prefix=/opt/nginx \
      --without-http_rewrite_module \
      --without-http_gzip_module \
      --add-module=../nginx-rtmp-module-${NGINX_RTMP_VERSION} \
 && make -j$(nproc) \
 && make install \
 && cp ../nginx-rtmp-module-${NGINX_RTMP_VERSION}/stat.xsl /opt/nginx/html/ \
 && ln -s /dev/stdout /opt/nginx/logs/access.log \
 && ln -s /dev/stderr /opt/nginx/logs/error.log

FROM debian:bullseye-slim

COPY --from=builder /opt/nginx /opt/nginx

COPY nginx.conf /opt/nginx/conf/nginx.conf

CMD ["/opt/nginx/sbin/nginx", "-g", "daemon off;"]
